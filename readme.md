# IBAMR docker recipe

This repository contains docker recipes for the
[IBAMR (Immersed Boundary Method Adaptive Mesh Refinement)](https://ibamr.github.io/)
simulation framework.

## Compatibility

The docker images may or may not run on your hardware as they include aggressive
optimization that takes advantage of CPU features not present on older hardware;
however the goal of the project is to provide a docker file you can use to build
a container with optimizations suited to the computers you'll run on.

## Containers

The containers are named according to the following scheme:

+ All containers have a version number in their suffix.
+ Containers tagged `dcthomp/ibamr:`**`version`**`-build-deps` have the build trees of IBAMR's dependencies but not IBAMR.
+ Containers tagged `dcthomp/ibamr:`**`version`**`-build` have the build trees of IBAMR and its dependencies.
+ Containers tagged `dcthomp/ibamr:`**`version`** are stripped of source and object files to save space.

Currently, there are only docker files for version 0.8 and they produce 3 containers:

+ `dcthomp/ibamr:0.8-build-deps` – this container can be used to build IBAMR as part of continuous integration to
   avoid rebuilding third-party dependencies of IBAMR.
+ `dcthomp/ibamr:0.8-build` – this container has a build of IBAMR that can be used to try out changes or do development.
+ `dcthomp/ibamr:0.8` – this container has only executables and is intended to be as minimal as possible.

All containers are laid out like so:

+ `/ibamr` – contains libraries and example programs you can run with your own input decks.
+ `/petsc` – holds PETsc libraries that IBAMR links to; it also holds local silo and hypre builds.
+ `/samrai` – holds SAMRAI libraries that IBAMR links to.
+ `/libmesh` – holds libmesh libraries that IBAMR links to.
+ `/numdiff` – holds the numdiff application that can be useful when validating IBAMR output.

Additionally, the `ibamr-build` and `ibamr-build-deps` containers have

+ `/build` – Build trees for IBAMR and its dependencies, except for PETsc, which is built
  in place in `/petsc`.

## Building

To build the containers, run `docker build` for each target in the dockerfile:

```sh
docker build -t dcthomp/ibamr:0.8-build-deps --target ibamr-build-deps-v0.8 0.8/build-deps
docker build -t dcthomp/ibamr:0.8-build --target ibamr-build-v0.8 0.8/build
docker build -t dcthomp/ibamr:0.8 --target ibamr-v0.8 0.8/minimal
```

You must run them in the order above as they build on one another.
